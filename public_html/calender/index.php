<?php
if(isset($_POST['submit'])){
require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secret.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  if(isset($_POST['submit'])){
  $summary = $_POST['summary'];
  $location = $_POST['location'];
  $description = $_POST['description'];
  $start = $_POST['start'];
  }
  $event = new Google_Service_Calendar_Event(array(
  'summary' => $summary,
  'location' => $location,
  'description' => $description,
  'start' => array(
	'dateTime' => $_POST['start'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  /*'start' => $start,*/
  
  'end' => array(
    'dateTime' =>  $_POST['end'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=2'
  ),
  'attendees' => array(
    array('email' => 'lpage@example.com'),
    array('email' => 'sbrin@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} else {
  $redirect_uri =  'http://liadni.myweb.jce.ac.il/calender/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}
}
?>
 <head>
    <meta charset="utf-8">  
	   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">  
    <link rel="stylesheet" href="style.css" type="text/css">  
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
  <nav class="navbar navbar-default" >
<a class="navbar-brand" href="#">Google Calender Api  By Liad Nizri</a>
</nav>
<html>
  <style>
    body {
        background: white }
    section {
        background: white;
        color: black;
		<!--border:1px solid black;-->
		box-shadow: 10px 10px 5px #888888;
        border-radius: 1em;
        padding: 1em;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
		 margin-top: 4%;
       transform: translate(-50%, -50%) }-->
  </style>
  
<section class="bs-docs-section">
<h2 style= "text-align:center">Create Evant</h2>
<form action="index.php" method="post">
  <div class="form-group">
    <label>Summary</label>
    <input type="text" class="form-control" placeholder="insert summary"  name = "summary">
  </div>
  <div class="form-group">
    <label >Location</label>
   <input type="text" class="form-control" placeholder="insert location" name = "location">
  </div>
  <div class="form-group">
    <label >Description</label>
    <input type="text" class="form-control" placeholder="insert description" name = "description">
  </div>
  <div class="form-group">
    <label >Start</label>
    <input type="datetime-local" class="form-control" name = "start">
  </div>
  <div class="form-group">
    <label >End</label>
    <input type="datetime-local" class="form-control" name = "end">
  </div>
 <!-- <div class="form-group">
    <label for="exampleInputEmail1">Partners Number</label>
    <input type="number" class="form-control" name = "partners">
  </div>-->
  
  <button type="submit" value="submit" name="submit" class="btn btn-success">Submit</button>
</form>
</section>


